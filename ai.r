source('evaluate.r')
alphabeta <- function(board,player,depth,alpha,beta) { 
  if(depth == DEPTH_LIMIT){
    return(evaluate(board))
  }
  #Find the best position of those available
  if(player == ME){
    child_boards <- generate_next_boards(board,ME)
    if(length(child_boards) == 0){
      ##No valid moves, we are at leaf of the tree. 
      return(evaluate(board))
    }
    for(next_board in child_boards){
      if(max(next_board) == 2048) { return(1000000000) }
      
      result <- alphabeta(next_board,ENEMY,depth+1,alpha,beta)
      if(result > alpha){
        alpha <- result
      }
      if(beta <= alpha){
        break ##prune - The lowest we will return is alpha, 
              ##but our parent (a min node -- ENEMY turn node) 
              ##has beta as its best (smaller) choice right now
      }
      return(alpha)
    }
  }
  ### Find the worst position of those available
  if(player == ENEMY){
    child_boards <- generate_next_boards(board,ENEMY)
    for(next_board in child_boards){
      result <- alphabeta(next_board,ME,depth+1,alpha,beta)
      if(result < beta){
        beta <- result
      }
      if(beta <= alpha){
        break ##prune - now the highest value we will return is beta,
              ##but our parent (a max node -- ME turn node)
              ##has alpha as its best (larger) choice right now.
              ##in other words: even if we find an *even worse* position
              ##than beta in the future, it doesn't matter because we know
              ##ME will choose the position from one of our siblings
      }
      return(beta)
    }
  }
}


generate_next_boards <- function(board,player){
  boards <- list()
  if(player == ME){
    for(move in 1:4){
      if(is_valid_move(board,move)){
        boards[[length(boards)+1]] <- swipe_board(board,move)
      }
    }
  }
  if(player == ENEMY){
    ##find all blank spots
    spots <- which(board == 0, arr.ind=T)
    i <- 1
    j <- 1
    #Q here: should you iterate over all possible vals, or 
    #make a random choice? Right now it is random 
    while(i <= nrow(spots)){
      for(val in c(2,4)){
        spot <- spots[i,]
        b <- board
        b[spot[[1]],spot[[2]]] <- val
        boards[[j]] <- b
        j <- j+1
      }
      i <- i+1
    }
  }
  return(boards)
}