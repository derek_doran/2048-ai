is_valid_move <- function(board,move){
  #if nothing changed, it is invalid
  b <- swipe_board(board,move)
  return(!all(b == board))
}
is_game_over <- function(board) { 
  if(is_valid_move(board,LEFT))
    return(FALSE)
  if(is_valid_move(board,RIGHT))
    return(FALSE)
  if(is_valid_move(board,DOWN))
    return(FALSE)
  if(is_valid_move(board,UP))
    return(FALSE)
  return(TRUE)
}

##board manipulation functions
get_blank_board <- function() { 
  return(matrix(nrow=4,ncol=4,rep(0,16)))
}

add_piece <- function(board){
  ##find a blank spot
  num_spots <- length(which(board == 0, arr.ind=T)[,1])
  if(num_spots > 0){
    idx <- sample(1:num_spots,1)
    spot <- which(board == 0,arr.ind=T)[idx,]
    val <- sample(c(2,2,2,4),1)
    board[spot[[1]],spot[[2]]] <- val
  }
  return(board)
}

swipe_board <- function(board,direction){
  if(direction == UP){
    #merge up tiles column wise, starting from bottom
    for(i in 2:4){
      for(j in 1:4){
        if(board[i,j] == board[i-1,j]){
          board[i,j] <- 0
          board[i-1,j] <- board[i-1,j]*2
        }
      }
    }
  }
  
  if(direction == DOWN){
    #now merge tiles column wise
    for(i in 3:1){
      for(j in 1:4){
        if(board[i,j] == board[i+1,j]){
          board[i,j] <- 0
          board[i+1,j] <- board[i+1,j]*2
        }
      }
    }
  }
  
  if(direction == RIGHT){
    #merge tiles row wise, starting from left
    for(i in 1:4){
      for(j in 3:1){
        if(board[i,j] == board[i,j+1]){
          board[i,j] <- 0
          board[i,j+1] <- board[i,j+1]*2
        }
      }
    }
  }
  
  if(direction == LEFT){
    #merge tiles row wise, starting from right.
    for(i in 1:4){
      for(j in 1:3){
        if(board[i,j] == board[i,j+1]){
          board[i,j] <- board[i,j+1]*2
          board[i,j+1] <- 0
        }
      }
    }
  }
  #clean up shifts tiles onto empty spaces after merging
  board <- clean_up(board,direction)
  return(board)
}

clean_up <- function(board,direction){
   if(direction == LEFT){
     for(i in 1:4){
      for(j in 1:4){
       if(board[i,j] == 0){
           swap_spots <- which(board[i,] > 0)
           spot <- swap_spots[swap_spots > j]
           if(length(spot) > 0){
             spot <- min(swap_spots[swap_spots > j])
             board[i,j] <- board[i,spot]
             board[i,spot] <- 0
           }
       }
      }
     }
   }
   if(direction == RIGHT){
     for(i in 1:4){
       for(j in 4:1){
         if(board[i,j] == 0){
           swap_spots <- which(board[i,] > 0)
           spot <- swap_spots[swap_spots < j]
           if(length(spot) > 0){
             spot <- max(swap_spots[swap_spots < j])
             board[i,j] <- board[i,spot]
             board[i,spot] <- 0
           }
         }
       }
     }
   }
   if(direction == UP){
     for(i in 1:4){
       for(j in 1:4){
         if(board[i,j] == 0){
           swap_spots <- which(board[,j] > 0)
           spot <- swap_spots[swap_spots > i]
           if(length(spot) > 0){
             spot <- min(swap_spots[swap_spots > i])
             board[i,j] <- board[spot,j]
             board[spot,j] <- 0
           }
         }
       }
     }
   }
   if(direction == DOWN){
     for(i in 4:1){
       for(j in 1:4){
         if(board[i,j] == 0){
           swap_spots <- which(board[,j] > 0)
           spot <- swap_spots[swap_spots < i]
           if(length(spot) > 0){
             spot <- max(swap_spots[swap_spots < i])
             board[i,j] <- board[spot,j]
             board[spot,j] <- 0
           }
         }
       }
     }
   }
      
  return(board)
}



  