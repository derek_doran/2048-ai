## Game searching constants
ME <- 1
ENEMY <- -1
DEPTH_LIMIT <- 8

## Game playing constants
UP <- 1
DOWN <- 2
LEFT <- 3
RIGHT <- 4

### Global constants
INFTY <- 9999999