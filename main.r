source('consts.r')
source('game_functions.r')
source('ai.r')
source('evaluate.r')
## initialize   
x <- c()
sorts <- c()
num0 <- c()
i <- 1
board <- add_piece(add_piece(get_blank_board()))
while(!is_game_over(board)){
  ## what are my next possible moves? 
  next_boards <- generate_next_boards(board,ME)
  best_board <- -Inf
  best_score <- -INFTY
  for(b in next_boards){
    score <- alphabeta(b,ENEMY,1,-INFTY,INFTY)
    if(score > best_score){
      best_board <- b
      best_score <- score
    }
  }
  board <- add_piece(best_board)
  print(board)
  sorts[i] <- (sorted_stat(board)/evaluate(board))
  x[i] <- (0.5*weighted_valid_moves(board)/evaluate(board))
  num0[i] <- 1.2*length(board[board==0])/evaluate(board)
  i <- i+1
  #Sys.sleep(0.5)
}
plot(sorts,ylim=c(0,1))
points(x,col="blue")
lines(num0,col="red")

run_random_moves <- function(){
  board <- add_piece(add_piece(get_blank_board()))
  while(!is_game_over(board)){
    move <- sample(1:4,1)
    if(is_valid_move(board,move)){
      board <- swipe_board(board,move)
      board <- add_piece(board)
      #print(board)
    }
  }
  return(board)
}