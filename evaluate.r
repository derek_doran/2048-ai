### Assign a score to a board position. The higher the better..
evaluate <- function(board){
  ## The more empty board spots, the better
  #return(length(board[board==0]))
  ## The larger the sum of all tiles, the better
  #return(sum(board))
  ## The larger the value of the largest tile, the better.
  #return((max(board)))
  sort <- sorted_stat(board)
  num0 <- length(board[board==0])
  moves <- count_valid_moves(board)
  w_moves <- weighted_valid_moves(board)
  top_hot <- hotness(board,T,1)
  c1_hot <- hotness(board,F,1)
  c2_hot <- hotness(board,F,2)
  c3_hot <- hotness(board,F,3)
  c4_hot <- hotness(board,F,4)
  
  score <- sort + 
    1.2*num0 + 
    0.5*w_moves +
    #max(board) + 
    #0.2*sides + 
    #top_hot +
    #c1_hot + c2_hot + c3_hot + c4_hot
    check2048(board)
  return(score)# + 0.2*num0)
}

check1024 <- function(board){
  if(max(board) >= 1024)
    return(1000000)
  else return(0)
}

check2048 <- function(board){
  if(max(board) >= 2048)
    return(1000000)
  else return(0)
}

log2 <- function(x){
  if(x == 0) return(0)
  return(log(x,base=2))
}
mtxlog2 <- function(m) { 
  return(matrix(sapply(m,log2),nrow=4))
}

vec_norm <- function(x) { return(sqrt(sum(x^2))) }

##this is a measure of how well a row or column is sorted. 
hotness <- function(board,isRow,idx){ 
  board <- mtxlog2(board)
  hotness <- 0
  if(isRow == TRUE){
    for(j in 1:3){
      hotness_desc <- hotness + (board[idx,j] - board[idx,j+1])
      hotness_asc <- hotness + (board[idx,j+1] - board[idx,j])
      if(board[idx,j] > board[idx,j+1]){
        #it's sorted, but the smaller the difference, the greater the hotness. 
        hotness_desc <- hotness_desc + 13
      } else {
        hotness_asc <- hotness_asc + 13
      }
    }  
  }
  else { 
    for(i in 1:3){
      hotness_desc <- hotness + (board[i,idx] - board[i+1,idx])
      hotness_asc <- hotness + (board[i+1,idx] - board[i,idx])
      if(board[i,idx] > board[i+1,idx]){
        #it's sorted, but the smaller the difference, the greater the hotness. 
        hotness_desc <- hotness + 13
      } else {
        hotness_asc <- hotness_asc + 13
      }
    }  
  }  
  return(max(hotness_desc,hotness_asc))
}

##How well sorted is the board? We want the pieces to be
## strictly increasing across rows and across columns.
sorted_stat <- function(board){
  board <- matrix(sapply(board,log2),nrow=4)
  inc_x <- 0
  inc_y <- 0
  dec_x <- 0
  dec_y <- 0
  for(i in 1:4){
    inc_x <- inc_x + dist(rbind(board[i,],sort(board[i,])),method = "euclidian")
    dec_x <- dec_x + dist(rbind(board[i,],sort(board[i,],decreasing = T)),method="euclidian")
    inc_y <- inc_y + dist(rbind(board[,i],sort(board[,i])),method="euclidian")
    dec_y <- dec_y + dist(rbind(board[,i],sort(board[,i],decreasing = T)),method="euclidian")
  }
  return(abs(inc_x-dec_x) + abs(dec_y - inc_y))
}

count_valid_moves <- function(b){
  c <- 0
  for(i in 1:3){
    for(j in 1:4){
      if(b[i,j] == b[i+1,j]){
        c <- c+1
      }
    }
  }
  for(i in 1:4){
    for(j in 1:3){
      if(b[i,j] == b[i,j+1]){
        c <- c+1
      }
    }
  }
  return(c)
}


weighted_valid_moves <- function(b){
    c <- 0
    for(i in 1:3){
      for(j in 1:4){
        if(b[i,j] == b[i+1,j]){
          c <- c+ log2(b[i,j])
        }
      }
    }
    for(i in 1:4){
      for(j in 1:3){
        if(b[i,j] == b[i,j+1]){
          c <- c+ log2(b[i,j])
        }
      }
    }
    return(c)
}